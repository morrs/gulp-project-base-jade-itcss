
//  Setup base Gulp plugins/dependencies
// ====================================================== //

    var gulp = require('gulp'),
        gulpLoadPlugins = require('gulp-load-plugins'),
        browserSync = require('browser-sync'),
        plugins = gulpLoadPlugins(),
        gutil = require('gulp-util');

// Paths object for quick reference and cleaner code
// ====================================================== //

    var dir = {
        dev: './dev/',
        dist: './dist/'
    }

    var paths = {

        jade: {
            watch: dir.dev + 'jade/**/*.jade',
            compile: dir.dev + 'jade/*.jade'
        },
        styles: {
            files: dir.dev + 'scss/**/*.scss',
            compiled: dir.dev + 'css/styles.css',
            output: 'styles.css',
            dest: dir.dev + 'css/',
            live: dir.dist + 'css/'
        },
        js: {
            files: dir.dev + 'js/*.js',
            src: dir.dev + 'js/',
            vendor: dir.dev + 'js/vendor/*.js',
            pre: dir.dev + 'js/scripts.js',
            post: 'init.js',
            built: dir.dev + 'js/init.js',
            live: dir.dist + 'js'
        },
        images: {
            files: dir.dev + 'img/*.+(jpg|png|gif)',
            live: dir.dist + 'img/'
        }

    }

// Tasks
// ====================================================== //

    // Construct jade templates
    gulp.task('jade', function () {
        gulp.src(paths.jade.compile)
        .pipe(plugins.plumber({
            errorHandler: function(err) {
                    return plugins.notify().write('❗️❗️❗️ Jade - template compiling failed');
                }
            }
        ))
        .pipe(plugins.jade({
          pretty: true,
        }))
        .pipe(gulp.dest(dir.dev))
        .pipe(plugins.notify('💪 Jade - template compiling success'))
        .pipe(browserSync.reload(
            {stream:true})
        );
    });

    // Compile SCSS and prefix css
    gulp.task('css', function () {
        gulp.src(paths.styles.files)
        .pipe(plugins.plumber({
            errorHandler: function(err) {
                gutil.beep();
                gutil.log(err);
                return plugins.notify().write('❗️❗️❗️❗️ SCSS - Compile failed, see console for details');
            }
        }))
        .pipe(plugins.sass())
        .pipe(plugins.autoprefixer('last 2 versions', 'ie 9'))
        .pipe(plugins.rename(paths.styles.output))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(plugins.notify({
            onLast: true,
            message: "👍  SCSS successfully compiled"
        }))
        .pipe(browserSync.reload({
            stream:true
        }))
    });

    // Minify CSS
    gulp.task('minify-css', function () {
        gulp.src(paths.styles.compiled)
            .pipe(plugins.minifyCss())
            .pipe(gulp.dest(paths.styles.live))
            .pipe(plugins.notify("👍  CSS successfully minified"));
    });

    // Concatenate JS
    gulp.task('js', function () {
        gulp.src([paths.js.vendor, paths.js.pre])
            .pipe(plugins.concat(paths.js.post))
            .pipe(gulp.dest(paths.js.src))
            .pipe(plugins.notify("👍  JS successfully compiled"))
            .pipe(browserSync.reload(
                {stream:true}
            ));
    });

    // Minify JS
    gulp.task('minify-js', function () {
        gulp.src(paths.js.built)
            .pipe(plugins.uglify())
            .pipe(gulp.dest(paths.js.live))
            .pipe(plugins.notify("👍  JS successfully minified"));
    });

    // Image optimisation
    gulp.task('images', function () {
        return gulp.src(paths.images.files)
            .pipe(plugins.imagemin({
                optimizationLevel: 4,
                progressive: true
            }))
        .pipe(gulp.dest(paths.images.live))
        .pipe(plugins.notify("👍  Images optimised"));
    });

    // Browser-sync task for starting the server.
    gulp.task('browser-sync', function() {

        var files = [
            dir.dev,
            paths.styles.compiled,
            paths.images.files,
            paths.js.compiled
        ];

        browserSync.init(null, {
            server: {
                baseDir: dir.dev
            }
        });

    });

// Actions
// ====================================================== //

    gulp.task('watch', ['browser-sync'],  function() {

        // Watch html files
        gulp.watch(paths.jade.watch, function() {
            gulp.run('jade');
        });

        // Watch scss files
        gulp.watch(paths.styles.files, function() {
            gulp.run('css');
        });

        // Watch js
        gulp.watch(paths.js.files, function() {
            gulp.run('js');
        });

    });

    // Compile site files
    gulp.task('build', ['jade', 'css', 'js']);

    // Run to optimise site for delivery */
    gulp.task('publish', ['minify-css', 'minify-js', 'images']);
