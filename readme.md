# Setup

* [Install Node](http://nodejs.org/download/)
* [Install Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md#getting-started)
* Clone the repo
* CD to repo's location in terminal
* Run **npm install**
* Run **gulp watch** command

## Project structure

The project base is split into **two** main directories - **dev** and **dist** which are sat at the root level of the project.

**Dev**(development) - The primary working directory, all build activity will take place here.

**Dist**(distribution) - The location of the completed build, all files here will be in their final form - compressed, minified and optimised ready for deployment. **Files should never be edited here.**

## Development

###HTML

 * html templates are created using the Jade templating language
 * Templating works on the principle of template inheritance in that layout templates are used and individual pages then extend from these, see [Jade lang](http://jade-lang.com/reference/inheritance/) for further information
 * All pages should be created within the **dev/jade** folder and any template files should be created in the **dev/jade/layouts** folder
 * Compiled html resides in the root of the **dev/** directory

###SCSS/CSS

* All style development takes place in the **dev/scss/** location
* SCSS structure and approach employs the ITCSS methodology to produce scalable and efficient CSS. See [manage large scale web projects with ITCSS](http://www.creativebloq.com/web-design/manage-large-scale-web-projects-new-css-architecture-itcss-41514731)  [ITCSS reference slides](http://csswizardry.net/talks/2014/11/itcss-dafed.pdf) for more information

### JS

* jQuery 2.1.1 is included by default
* All scripting to be done in the **dev/js/scripts.js** file, within the provided document ready function
* Any third-party pluggins or libraries can be used and must be placed into the **dev/js/vendor** folder, these are then concatenated, along with jQuery and the scripts.js file to create **dev/js/init.js**, this is the file that the html references in the foooter
* All scripting must be done in the **dev/js/scripts.js** file, the **dev/js/init.js** should never be touched as changed will only be overwritten when the js compiling takes place

## Commands

* **Gulp watch**
 * Sets gulp to watch for changes in either the **html**, **scss** or the **js** files
 * will automatically update the browser unless an error is encountered
 * Produces feedback(success or failure compilation reports) via Mac OS X notifications
* **Gulp build**
 * Compiles the **html**, **scss** or the **js** files, performs the same action in regards to file compilation that **Gulp watch** carries out with the only difference being that this is a manual initiation
* **Gulp publish**
 * Takes the build files located in the **dev/** directory and prepares them for deployment - concatenation and minification of js, minification of css, images are also copied over and optimised

## References
* [Gulp task runner](http://gulpjs.com/) - Build system automating tasks
* [Modernizr](http://modernizr.com/) - Modernizr is a JavaScript library that detects HTML5 and CSS3 features in the user’s browser
* [Jade templating language ](http://jade-lang.com/) - Jade templating language
* [jQuery](https://jquery.com/) - Javascript library
* [ITCSS](http://csswizardry.net/talks/2014/11/itcss-dafed.pdf) -  A sane, scalable, managed. CSS architecture
